package gds.com.studentinformation;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Objects;

import gds.com.studentinformation.adapters.StudentAdapter;
import gds.com.studentinformation.adapters.listener.StudentItemClickListener;
import gds.com.studentinformation.models.Admin;
import gds.com.studentinformation.models.Student;
import gds.com.studentinformation.presenters.profiler.admin.AdminHomePresenter;
import gds.com.studentinformation.presenters.profiler.admin.AdminHomePresenterImpl;

import static gds.com.studentinformation.utils.Constants.COLUMN_EMAIL;
import static gds.com.studentinformation.utils.Constants.showExitDialog;

public class AdminProfileActivity extends AppCompatActivity implements AdminHomePresenter.View,
        SwipeRefreshLayout.OnRefreshListener, StudentItemClickListener, View.OnClickListener {

    CoordinatorLayout coordinatorLayout;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView adminName, studentsHeader;
    ImageView emptyStudentsImage;
    RecyclerView recyclerView;

    FloatingActionButton addStudent;

    Intent adminData;
    String adminEmail;

    ProgressDialog progressDialog;

    AdminHomePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_profile);

        adminData = getIntent();
        adminEmail = adminData.getStringExtra(COLUMN_EMAIL);
        Log.d("adminEmail", adminEmail);

        progressDialog = new ProgressDialog(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Hello, Admin");

        presenter = new AdminHomePresenterImpl(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.admin_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                presenter.userLogOut();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        showExitDialog(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart(adminEmail);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void initViews() {
        coordinatorLayout = findViewById(R.id.adminProfileCoor);
        swipeRefreshLayout = findViewById(R.id.adminDetailSwipe);
        recyclerView = findViewById(R.id.rvStudents);
        adminName = findViewById(R.id.tvAdminName);
        swipeRefreshLayout.setOnRefreshListener(this);
        studentsHeader = findViewById(R.id.tvStudentsHeader);
        emptyStudentsImage = findViewById(R.id.ivNoStudents);
        addStudent = findViewById(R.id.fabAddStudent);
        addStudent.setOnClickListener(this);
    }

    @Override
    public void showProfileViews() {
        adminName.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProfileViews() {
        adminName.setVisibility(View.GONE);
    }

    @Override
    public void showProgress(String title, String caption) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(caption);
        progressDialog.show();
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showSuccessDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("FIND STUDENTS", (dialog, which) -> presenter.requestStudents());
        builder.create().show();
    }

    @Override
    public void showErrorDialog(String title, String message, Intent intent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("DISMISS", (dialog, which) -> {
            startActivity(intent);
            finish();
        });
        builder.create().show();
    }

    @Override
    public void setUserInfo(Admin admin) {
        adminName.setText(String.format("%s, %s %s", admin.getLastName(), admin.getFirstName(), admin.getMiddleName()));
    }

    @Override
    public void loadStudents(List<Student> students) {
        emptyStudentsImage.setVisibility(View.GONE);
        studentsHeader.setVisibility(View.GONE);

        StudentAdapter adapter = new StudentAdapter(this, students, this);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setEmptyState(String emptyMessage) {
        recyclerView.setVisibility(View.GONE);

        emptyStudentsImage.setVisibility(View.VISIBLE);
        studentsHeader.setVisibility(View.VISIBLE);

        studentsHeader.setText(emptyMessage);
    }

    @Override
    public void onRefresh() {
        presenter.requestUserInfo(adminEmail);
    }

    @SuppressLint("InflateParams")
    @Override
    public void onClick(Student student) {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.student_profile_item, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(null);
        builder.setPositiveButton("CLOSE", (dialog, which) -> dialog.dismiss());
        builder.setNegativeButton("UPDATE", (dialog, which) -> {
            Intent updateIntent = new Intent(this, UpdateStudentActivity.class);
            updateIntent.putExtra("student_email", student.getEmail());
            updateIntent.putExtra(COLUMN_EMAIL, adminEmail);
            startActivity(updateIntent);
            finish();
            dialog.dismiss();
        });
        builder.setNeutralButton("DELETE", (dialog, which) -> {
            AlertDialog.Builder deleteBuilder = new AlertDialog.Builder(this);
            deleteBuilder.setCancelable(false);
            deleteBuilder.setTitle("Remove Student");
            deleteBuilder.setMessage("Do you want to proceed?");
            deleteBuilder.setPositiveButton("YES", (dialog1, which1) -> {
                presenter.removeStudent(student.getEmail(), student.getStudentId());
                dialog1.dismiss();
            }).setNegativeButton("CANCEL", (dialog12, which12) -> dialog12.dismiss());
            deleteBuilder.create().show();
        });
        builder.setView(dialogView);
        AlertDialog dialog = builder.create();

        TextView studentName = dialogView.findViewById(R.id.tvStudentName);
        TextView program = dialogView.findViewById(R.id.tvStudentProgram);
        TextView birthday = dialogView.findViewById(R.id.tvStudentBirthday);
        TextView age = dialogView.findViewById(R.id.tvStudentAge);
        TextView address = dialogView.findViewById(R.id.tvStudentAddress);
        TextView email = dialogView.findViewById(R.id.tvStudentEmail);

        studentName.setText(String.format("%s, %s %s", student.getLastName(), student.getFirstName(), student.getMiddleName()));
        program.setText(String.format("%s | %s Year", student.getProgram(), student.getYearLevel()));
        birthday.setText(student.getBirthday());
        age.setText(student.getAge());
        address.setText(student.getAddress());
        email.setText(student.getEmail());

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabAddStudent:
                Intent adminIntent = new Intent(this, CreateStudentActivity.class);
                adminIntent.putExtra(COLUMN_EMAIL, adminEmail);
                startActivity(adminIntent);
                finish();
                break;
        }
    }
}
