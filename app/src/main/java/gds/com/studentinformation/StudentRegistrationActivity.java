package gds.com.studentinformation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;
import java.util.Objects;

import gds.com.studentinformation.presenters.register.student.StudentRegisterPresenter;
import gds.com.studentinformation.presenters.register.student.StudentRegisterPresenterImpl;

public class StudentRegistrationActivity extends AppCompatActivity implements StudentRegisterPresenter.View {

    CoordinatorLayout coordinatorLayout;

    EditText firstName, middleName, lastName, birthday, age, address, email, password;
    Spinner program, yearLvl;

    ProgressDialog progressDialog;

    StudentRegisterPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_registration);

        progressDialog = new ProgressDialog(this);

        presenter = new StudentRegisterPresenterImpl(this, this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Create Student Account");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                presenter.submitCredentials(
                        firstName.getText().toString(), middleName.getText().toString(),
                        lastName.getText().toString(), program.getSelectedItem().toString(),
                        yearLvl.getSelectedItem().toString(), email.getText().toString(),
                        birthday.getText().toString(), age.getText().toString(),
                        address.getText().toString(), password.getText().toString());
                break;
            case android.R.id.home:
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void initViews() {
        coordinatorLayout = findViewById(R.id.studentRegCoor);
        firstName = findViewById(R.id.etFirstName);
        middleName = findViewById(R.id.etMiddleName);
        lastName = findViewById(R.id.etLastName);
        birthday = findViewById(R.id.etBirthday);
        age = findViewById(R.id.etAge);
        address = findViewById(R.id.etAddress);
        email = findViewById(R.id.etEmail);
        password = findViewById(R.id.etPassword);

        program = findViewById(R.id.spProgram);
        yearLvl = findViewById(R.id.spYearLvl);
    }

    @Override
    public void showSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress(String title, String caption) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(caption);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showSuccessDialog(String successTitle, String successMessage, Intent studentData) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(successTitle);
        builder.setMessage(successMessage);
        builder.setPositiveButton("DISMISS", (dialog, which) -> {
            startActivity(studentData);
            finish();
        });
        builder.create().show();
    }

    @Override
    public void showErrorDialog(String errorTitle, String errorMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(errorTitle);
        builder.setMessage(errorMessage);
        builder.setPositiveButton("DISMISS", (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }

    @Override
    public void showExitDialog(String exitTitle, String exitMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(exitTitle);
        builder.setMessage(exitMessage);
        builder.setPositiveButton("EXIT", (dialog, which) -> {
            startActivity(new Intent(StudentRegistrationActivity.this, LoginActivity.class));
            finish();
        });
        builder.create().show();
    }

    @Override
    public void setProgramOptions(List<String> programOptions) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, programOptions);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        program.setAdapter(adapter);
        program.setPrompt("Select Your Program");
    }

    @Override
    public void setYearLevelOptions(List<String> yearLevelOptions) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, yearLevelOptions);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        yearLvl.setAdapter(adapter);
        yearLvl.setPrompt("Select Year Level");
    }
}
