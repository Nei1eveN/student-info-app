package gds.com.studentinformation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.Objects;

import gds.com.studentinformation.models.Student;
import gds.com.studentinformation.presenters.profiler.student.StudentHomePresenter;
import gds.com.studentinformation.presenters.profiler.student.StudentHomePresenterImpl;

import static gds.com.studentinformation.utils.Constants.COLUMN_EMAIL;
import static gds.com.studentinformation.utils.Constants.showExitDialog;

public class StudentHomeActivity extends AppCompatActivity implements StudentHomePresenter.View, SwipeRefreshLayout.OnRefreshListener {

    CoordinatorLayout coordinatorLayout;
    SwipeRefreshLayout swipeRefreshLayout;

    TextView studentName, studentProgram, tvStudentBirthday, tvStudentAge, tvStudentAddress, tvStudentEmail;

    Intent studentData;

    ProgressDialog progressDialog;

    StudentHomePresenter presenter;

    String studentEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_home);

        studentData = getIntent();
        studentEmail = studentData.getStringExtra(COLUMN_EMAIL);

        progressDialog = new ProgressDialog(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Hello, Student");

        presenter = new StudentHomePresenterImpl(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.student_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.editProfile:
                Intent updateIntent = new Intent(this, StudentEditProfileActivity.class);
                updateIntent.putExtra(COLUMN_EMAIL, studentEmail);
                startActivity(updateIntent);
                finish();
                break;
            case R.id.logout:
                presenter.userLogOut();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        showExitDialog(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("studentEmail", studentEmail);
        presenter.onStart(studentEmail);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showViews() {
        studentName.setVisibility(View.VISIBLE);
        studentProgram.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideViews() {
        studentName.setVisibility(View.GONE);
        studentProgram.setVisibility(View.GONE);
    }

    @Override
    public void showProgress(String title, String caption) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(caption);
        progressDialog.show();
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showErrorDialog(String title, String message, Intent intent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("DISMISS", (dialog, which) -> {
            startActivity(intent);
            finish();
        });
        builder.create().show();
    }

    @Override
    public void initViews() {
        coordinatorLayout = findViewById(R.id.studentDetailCoor);
        studentName = findViewById(R.id.tvUserName);
        studentProgram = findViewById(R.id.tvProgram);
        tvStudentBirthday = findViewById(R.id.tvStudentBirthday);
        tvStudentAge = findViewById(R.id.tvStudentAge);
        tvStudentAddress = findViewById(R.id.tvStudentAddress);
        tvStudentEmail = findViewById(R.id.tvStudentEmail);

        swipeRefreshLayout = findViewById(R.id.detailSwipe);
        swipeRefreshLayout.setOnRefreshListener(this);

    }

    @Override
    public void setUserInfo(Student student) {
        studentName.setText(String.format("%s, %s %s", student.getLastName(), student.getFirstName(), student.getMiddleName()));
        studentProgram.setText(String.format("%s | %s Year", student.getProgram(), student.getYearLevel()));
        tvStudentBirthday.setText(student.getBirthday());
        tvStudentAge.setText(student.getAge());
        tvStudentAddress.setText(student.getAddress());
        tvStudentEmail.setText(student.getEmail());
    }

    @Override
    public void onRefresh() {
        presenter.requestUserInfo(studentEmail);
    }
}
