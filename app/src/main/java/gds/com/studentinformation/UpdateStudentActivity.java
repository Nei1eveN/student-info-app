package gds.com.studentinformation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.List;
import java.util.Objects;

import gds.com.studentinformation.models.Student;
import gds.com.studentinformation.presenters.update.admin.UpdateStudentPresenter;
import gds.com.studentinformation.presenters.update.admin.UpdateStudentPresenterImpl;

import static gds.com.studentinformation.utils.Constants.COLUMN_EMAIL;

public class UpdateStudentActivity extends AppCompatActivity implements UpdateStudentPresenter.View {

    Intent intent;
    String adminEmail, studentEmail;

    CoordinatorLayout updateStudentCoor;

    TextInputLayout txFirstName, txMiddleName, txLastName, txBirthday, txAge, txAddress;
    TextInputEditText etFirstName, etMiddleName, etLastName, etBirthday, etAge, etAddress;

    Spinner spProgram, spYearLevel;

    ProgressDialog progressDialog;

    UpdateStudentPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_student);

        intent = getIntent();
        adminEmail = intent.getStringExtra(COLUMN_EMAIL);
        studentEmail = intent.getStringExtra("student_email");

        progressDialog = new ProgressDialog(this);

        presenter = new UpdateStudentPresenterImpl(this, this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Update Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart(studentEmail, adminEmail);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                presenter.submitUpdateDetails(studentEmail, adminEmail,
                        etFirstName.getText().toString(), etMiddleName.getText().toString(),
                        etLastName.getText().toString(), spProgram.getSelectedItem().toString(),
                        spYearLevel.getSelectedItem().toString(), etBirthday.getText().toString(),
                        etAge.getText().toString(), etAddress.getText().toString());
                break;
            case android.R.id.home:
               Intent adminIntent = new Intent(this, AdminProfileActivity.class);
               adminIntent.putExtra(COLUMN_EMAIL, adminEmail);
               startActivity(adminIntent);
               finish();
               break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent adminIntent = new Intent(this, AdminProfileActivity.class);
        adminIntent.putExtra(COLUMN_EMAIL, adminEmail);
        startActivity(adminIntent);
        finish();
    }

    @Override
    public void initViews() {
        updateStudentCoor = findViewById(R.id.updateStudentCoor);
        txFirstName = findViewById(R.id.txFirstName);
        txMiddleName = findViewById(R.id.txMiddleName);
        txLastName = findViewById(R.id.txLastName);
        txBirthday = findViewById(R.id.txBirthday);
        txAge = findViewById(R.id.txAge);
        txAddress = findViewById(R.id.txAddress);

        etFirstName = findViewById(R.id.etFirstName);
        etMiddleName = findViewById(R.id.etMiddleName);
        etLastName = findViewById(R.id.etLastName);
        etBirthday = findViewById(R.id.etBirthday);
        etAge = findViewById(R.id.etAge);
        etAddress = findViewById(R.id.etAddress);

        spProgram = findViewById(R.id.spProgram);
        spYearLevel = findViewById(R.id.spYearLevel);
    }

    @Override
    public void showProgress(String title, String message) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showSuccessDialog(String title, String message, Intent successIntent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("DISMISS", (dialog, which) -> {
            startActivity(successIntent);
            finish();
        });
        builder.create().show();
    }

    @Override
    public void showErrorDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("DISMISS", (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }

    @Override
    public void showExitDialog(String title, String message, Intent errorIntent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("EXIT", (dialog, which) -> {
            startActivity(errorIntent);
            finish();
        });
        builder.create().show();
    }

    @Override
    public void setUserInfo(Student student) {
        etFirstName.setText(student.getFirstName());
        etMiddleName.setText(student.getMiddleName());
        etLastName.setText(student.getLastName());
        etBirthday.setText(student.getBirthday());
        etAge.setText(student.getAge());
        etAddress.setText(student.getAddress());
    }

    @Override
    public void setProgramOptions(List<String> programs) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, programs);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spProgram.setPrompt("Select Program");
        spProgram.setAdapter(adapter);
    }

    @Override
    public void setYearLevelOptions(List<String> yearLevels) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, yearLevels);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spYearLevel.setPrompt("Select Year Level");
        spYearLevel.setAdapter(adapter);
    }
}
