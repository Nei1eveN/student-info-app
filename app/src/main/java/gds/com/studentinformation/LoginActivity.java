package gds.com.studentinformation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import gds.com.studentinformation.presenters.login.LoginPresenter;
import gds.com.studentinformation.presenters.login.LoginPresenterImpl;

import static gds.com.studentinformation.utils.Constants.REGISTER_AS_OPTIONS;
import static gds.com.studentinformation.utils.Constants.showExitDialog;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, LoginPresenter.View {

    EditText email, password;
    Button login, register;
    Spinner userType;

    LoginPresenter presenter;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.etEmail);
        password = findViewById(R.id.etPassword);
        userType = findViewById(R.id.spUserType);
        login = findViewById(R.id.btnLogin);
        register = findViewById(R.id.btnRegister);

        progressDialog = new ProgressDialog(this);

        login.setOnClickListener(this);
        register.setOnClickListener(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Please Login");

        presenter = new LoginPresenterImpl(this, this);
    }

    @Override
    public void onBackPressed() {
        showExitDialog(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                presenter.submitCredentials(email.getText().toString(), password.getText().toString(), userType.getSelectedItem().toString());
                break;
            case R.id.btnRegister:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(false);
                builder.setTitle("Register As");
                builder.setItems(REGISTER_AS_OPTIONS, (dialog, which) -> {
                    switch (which) {
                        case 0:
                            presenter.findStudent();
                            break;
                        case 1:
                            presenter.findAdmin();
                            break;
                    }
                });
                builder.setPositiveButton("Cancel", (dialog, which) -> dialog.dismiss());
                AlertDialog dialog = builder.create();
                dialog.show();
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void showProgress(String title, String caption) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(caption);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadItemsToSpinner(List<String> options) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, options);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        userType.setAdapter(adapter);
        userType.setPrompt("Login As");
    }

    @Override
    public void showErrorDialog(String errorTitle, String errorMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(errorTitle);
        builder.setMessage(errorMessage);
        builder.setPositiveButton("DISMISS", (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }

    @Override
    public void goToMainActivity(Intent userData) {
        startActivity(userData);
        finish();
    }

    @Override
    public void goToStudentRegister() {
        startActivity(new Intent(this, StudentRegistrationActivity.class));
    }

    @Override
    public void goToAdminRegister() {
        startActivity(new Intent(this, AdminRegisterActivity.class));
    }
}
