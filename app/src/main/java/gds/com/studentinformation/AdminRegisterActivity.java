package gds.com.studentinformation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import java.util.Objects;

import gds.com.studentinformation.presenters.register.admin.AdminRegisterPresenter;
import gds.com.studentinformation.presenters.register.admin.AdminRegisterPresenterImpl;

public class AdminRegisterActivity extends AppCompatActivity implements AdminRegisterPresenter.View {

    CoordinatorLayout coordinatorLayout;

    EditText firstName, middleName, lastName, email, password;
    ProgressDialog progressDialog;

    AdminRegisterPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_register);

        progressDialog = new ProgressDialog(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Create Admin Account");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new AdminRegisterPresenterImpl(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                presenter.submitCredentials(firstName.getText().toString(), middleName.getText().toString(), lastName.getText().toString(), email.getText().toString(), password.getText().toString());
                break;
            case android.R.id.home:
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void initViews() {
        coordinatorLayout = findViewById(R.id.adminRegCoor);
        firstName = findViewById(R.id.etFirstName);
        middleName = findViewById(R.id.etMiddleName);
        lastName = findViewById(R.id.etLastName);
        email = findViewById(R.id.etEmail);
        password = findViewById(R.id.etPassword);
    }

    @Override
    public void showSnackMessage(String snackMessage) {
        Snackbar.make(coordinatorLayout, snackMessage, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress(String title, String caption) {
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(caption);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public void showSuccessDialog(String successTitle, String successMessage, Intent adminData) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(successTitle);
        builder.setMessage(successMessage);
        builder.setPositiveButton("DISMISS", (dialog, which) -> {
            startActivity(adminData);
            finish();
        });
        builder.create().show();
    }

    @Override
    public void showErrorDialog(String errorTitle, String errorMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(errorTitle);
        builder.setMessage(errorMessage);
        builder.setPositiveButton("DISMISS", (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }

    @Override
    public void showExitDialog(String exitTitle, String exitMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(exitTitle);
        builder.setMessage(exitMessage);
        builder.setPositiveButton("EXIT", (dialog, which) -> {
            startActivity(new Intent(AdminRegisterActivity.this, LoginActivity.class));
            finish();
        });
        builder.create().show();
    }
}
