package gds.com.studentinformation.presenters.register.student;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import gds.com.studentinformation.StudentHomeActivity;
import gds.com.studentinformation.models.Result;
import gds.com.studentinformation.models.Student;
import gds.com.studentinformation.utils.SimpleCrypto;
import gds.com.studentinformation.utils.retrofit.APIClient;
import gds.com.studentinformation.utils.retrofit.APIService;
import gds.com.studentinformation.utils.sqlite.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static gds.com.studentinformation.utils.Constants.COLUMN_ADDRESS;
import static gds.com.studentinformation.utils.Constants.COLUMN_AGE;
import static gds.com.studentinformation.utils.Constants.COLUMN_BIRTHDAY;
import static gds.com.studentinformation.utils.Constants.COLUMN_EMAIL;
import static gds.com.studentinformation.utils.Constants.COLUMN_ENCRYPTED_PASSWORD;
import static gds.com.studentinformation.utils.Constants.COLUMN_FIRSTNAME;
import static gds.com.studentinformation.utils.Constants.COLUMN_LASTNAME;
import static gds.com.studentinformation.utils.Constants.COLUMN_MIDDLENAME;
import static gds.com.studentinformation.utils.Constants.COLUMN_PASSWORD;
import static gds.com.studentinformation.utils.Constants.COLUMN_PROGRAM;
import static gds.com.studentinformation.utils.Constants.COLUMN_REGISTRATION_TIMESTAMP;
import static gds.com.studentinformation.utils.Constants.COLUMN_USER_ROLE;
import static gds.com.studentinformation.utils.Constants.COLUMN_YEAR_LEVEL;
import static gds.com.studentinformation.utils.Constants.STUDENT_TABLE_NAME;
import static gds.com.studentinformation.utils.Constants.USER_TABLE_NAME;
import static gds.com.studentinformation.utils.Constants.isNetworkAvailable;
import static gds.com.studentinformation.utils.Constants.registrationTimeFormat;

class StudentRegisterInteractorImpl implements StudentRegisterInteractor {
    private Context context;

    StudentRegisterInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getProgramOptions(ProgramListener listener) {
        List<String> programs = new ArrayList<>();
        programs.add("Associate in Computer Technology");
        programs.add("Computer Science");
        programs.add("Information Systems");
        programs.add("Information Technology");

        listener.onProgramsLoaded(programs);
    }

    @Override
    public void getYearLevelOptions(YearLevelsListener listener) {
        List<String> yearLevels = new ArrayList<>();
        yearLevels.add("1st");
        yearLevels.add("2nd");
        yearLevels.add("3rd");
        yearLevels.add("4th");
        listener.onYearLevelsLoaded(yearLevels);
    }

    @Override
    public void getStudentCredentials(String firstName, String middleName, String lastName,
                                      String program, String yearLevel, String email,
                                      String birthday, String age, String address, String password, StudentRegisterListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onRegisterFailure("Network Error", "Please check your internet connection");
        } else {
            APIService registerService = APIClient.getClient(context).create(APIService.class);

            Call<Result> studentCall = registerService.saveStudentCredentials
                    (firstName, middleName, lastName,
                            program, yearLevel, email,
                            birthday, age, address,
                            registrationTimeFormat.format(new Date()));

            studentCall.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                    if (Objects.requireNonNull(response.body()).getError()) {
                        listener.onRegisterFailure("Existing Email", response.body().getErrorMessage());
                    } else {
                        Result result = response.body();
                        Call<Result> userCall = registerService.saveUserCredentials(email, password, password, "Student");
                        userCall.enqueue(new Callback<Result>() {
                            @Override
                            public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                                DatabaseHelper databaseHelper = new DatabaseHelper(context);
                                SQLiteDatabase database = databaseHelper.getWritableDatabase();

                                database.beginTransaction();
                                try {
                                    ContentValues studentValues = new ContentValues();
                                    studentValues.put(COLUMN_FIRSTNAME, firstName);
                                    studentValues.put(COLUMN_MIDDLENAME, middleName);
                                    studentValues.put(COLUMN_LASTNAME, lastName);
                                    studentValues.put(COLUMN_PROGRAM, program);
                                    studentValues.put(COLUMN_YEAR_LEVEL, yearLevel);
                                    studentValues.put(COLUMN_EMAIL, email);
                                    studentValues.put(COLUMN_BIRTHDAY, birthday);
                                    studentValues.put(COLUMN_AGE, age);
                                    studentValues.put(COLUMN_ADDRESS, address);
                                    studentValues.put(COLUMN_REGISTRATION_TIMESTAMP, registrationTimeFormat.format(new Date()));

                                    ContentValues userValues = new ContentValues();
                                    userValues.put(COLUMN_EMAIL, email);
                                    userValues.put(COLUMN_PASSWORD, password);
                                    userValues.put(COLUMN_ENCRYPTED_PASSWORD, new SimpleCrypto().encrypt("encrypted_password", password));
                                    userValues.put(COLUMN_USER_ROLE, "Student");

                                    database.insertWithOnConflict(USER_TABLE_NAME, null, userValues, SQLiteDatabase.CONFLICT_NONE);
                                    database.insertWithOnConflict(STUDENT_TABLE_NAME, null, studentValues, SQLiteDatabase.CONFLICT_NONE);
                                    database.setTransactionSuccessful();
                                } finally {
                                    database.endTransaction();
                                }
                                Student student = result.getStudent();

                                Intent studentData = new Intent(context, StudentHomeActivity.class);
                                studentData.putExtra(COLUMN_EMAIL, student.getEmail());

                                listener.onRegisterSuccess(
                                        "Student Registered",
                                        "Registration Successful",
                                        studentData);
                            }

                            @Override
                            public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                                listener.onRegisterFailure("Registration Error", t.getMessage());
                            }
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                    listener.onRegisterFailure("Saving Error", t.getMessage());
                }
            });


        }
    }
}
