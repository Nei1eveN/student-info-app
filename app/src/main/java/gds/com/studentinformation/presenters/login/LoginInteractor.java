package gds.com.studentinformation.presenters.login;

import android.content.Intent;

import java.util.List;

public interface LoginInteractor {
    interface FindUserListener {
        void onUserFound(String successMessage, Intent userData);

        void onUserEmpty(List<String> loginAsOptions);
    }

    interface LoginCredentialsListener {
        void onLoginSuccess(String successMessage, Intent userData);

        void onLoginFailure(String errorTitle, String errorMessage);
    }

    interface FindAdminListener {
        void onAdminFound(String successTitle, String successMessage);
        void onAdminNotFound();
    }

    void getUser(FindUserListener listener);

    void checkAdminIfExisting(FindAdminListener listener);

    void getLoginCredentials(String email, String password, String selectedUserType, LoginCredentialsListener listener);
}
