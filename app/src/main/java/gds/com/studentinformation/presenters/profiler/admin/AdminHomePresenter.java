package gds.com.studentinformation.presenters.profiler.admin;

import android.content.Intent;

import java.util.List;

import gds.com.studentinformation.models.Admin;
import gds.com.studentinformation.models.Student;

public interface AdminHomePresenter {
    interface View {
        void initViews();
        void showProfileViews();
        void hideProfileViews();
        void showProgress(String title, String caption);
        void hideProgress();
        void showSuccessDialog(String title, String message);
        void showErrorDialog(String title, String message, Intent intent);
        void setUserInfo(Admin admin);
        void loadStudents(List<Student> students);
        void setEmptyState(String emptyMessage);
    }
    void onStart(String email);
    void onDestroy();
    void requestUserInfo(String email);
    void requestStudents();
    void removeStudent(String studentEmail, Integer studentId);
    void userLogOut();
}
