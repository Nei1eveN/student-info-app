package gds.com.studentinformation.presenters.register.student;

import android.content.Context;
import android.content.Intent;

import java.util.List;

public class StudentRegisterPresenterImpl implements StudentRegisterPresenter,
        StudentRegisterInteractor.StudentRegisterListener, StudentRegisterInteractor.ProgramListener, StudentRegisterInteractor.YearLevelsListener {
    private StudentRegisterPresenter.View view;
    private StudentRegisterInteractor interactor;

    public StudentRegisterPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentRegisterInteractorImpl(context);
    }

    @Override
    public void onStart() {
        if (view != null) {
            view.initViews();
        }
        interactor.getProgramOptions(this);
        interactor.getYearLevelOptions(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void submitCredentials(String firstName, String middleName, String lastName, String program, String yearLevel, String email, String birthday, String age, String address, String password) {
        if (firstName.isEmpty()) {
            view.showErrorDialog("First Name Field Empty", "Please fill in the required field.");
            view.showSnackMessage("Please fill in the required field.");
        } else if (middleName.isEmpty()) {
            view.showErrorDialog("Middle Name Field Empty", "Please fill in the required field.\n\nYou may just type N/A if you do not have a Middle Name.");
            view.showSnackMessage("Please fill in the required field.");
        } else if (lastName.isEmpty()) {
            view.showErrorDialog("Last Name Field Empty", "Please fill in the required field.");
            view.showSnackMessage("Please fill in the required field.");
        } else if (program.isEmpty()) {
            view.showErrorDialog("Program Field Empty", "Please fill in the required field.");
            view.showSnackMessage("Please fill in the required field.");
        } else if (yearLevel.isEmpty()) {
            view.showErrorDialog("Year Level Field Empty", "Please fill in the required field.");
            view.showSnackMessage("Please fill in the required field.");
        } else if (birthday.isEmpty()) {
            view.showErrorDialog("Birthday Field Empty", "Please fill in the required field.");
            view.showSnackMessage("Please fill in the required field.");
        } else if (age.isEmpty()) {
            view.showErrorDialog("Age Field Empty", "Please fill in the required field.");
            view.showSnackMessage("Please fill in the required field.");
        } else if (address.isEmpty()) {
            view.showErrorDialog("Address Field Empty", "Please fill in the required field.");
            view.showSnackMessage("Please fill in the required field.");
        } else if (email.isEmpty()) {
            view.showErrorDialog("Email Field Empty", "Please fill in the required field.");
            view.showSnackMessage("Please fill in the required field.");
        } else if (password.isEmpty()) {
            view.showErrorDialog("Password Field Empty", "Please fill in the required field.");
            view.showSnackMessage("Please fill in the required field.");
        } else if (password.length() < 8) {
            view.showErrorDialog("Insufficient Password Length", "Please create a password with at least 8 characters.");
        } else {
            if (view != null) {
                view.showProgress("Saving Data", "Please wait...");
                interactor.getStudentCredentials(firstName, middleName, lastName, program, yearLevel, email, birthday, age, address, password, this);
            }
        }
    }

    @Override
    public void onRegisterSuccess(String successTitle, String successMessage, Intent studentData) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(successTitle, successMessage);
            view.showSnackMessage(successTitle);
//            view.showSuccessDialog(successTitle, successMessage, studentData);
        }
    }

    @Override
    public void onRegisterFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage);
            view.showSnackMessage(errorTitle);
        }
    }

    @Override
    public void onProgramsLoaded(List<String> programs) {
        if (view != null) {
            view.setProgramOptions(programs);
        }
    }

    @Override
    public void onYearLevelsLoaded(List<String> yearLevels) {
        if (view != null) {
            view.setYearLevelOptions(yearLevels);
        }
    }
}
