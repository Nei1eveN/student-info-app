package gds.com.studentinformation.presenters.create;

import android.content.Intent;

import java.util.List;

public interface CreateStudentPresenter {
    interface View {
        void initViews();
        void showSnackMessage(String snackMessage);
        void showProgress(String title, String caption);
        void hideProgress();
        void showSuccessDialog(String successTitle, String successMessage, Intent adminData);
        void showErrorDialog(String errorTitle, String errorMessage);
        void showExitDialog(String exitTitle, String exitMessage);
        void setProgramOptions(List<String> programOptions);
        void setYearLevelOptions(List<String> yearLevelOptions);
    }
    void onStart();
    void onDestroy();
    void submitCredentials(String adminEmail, String firstName, String middleName, String lastName,
                           String program, String yearLevel, String email,
                           String birthday, String age, String address, String password);
}
