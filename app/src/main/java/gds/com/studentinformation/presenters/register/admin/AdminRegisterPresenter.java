package gds.com.studentinformation.presenters.register.admin;

import android.content.Intent;

public interface AdminRegisterPresenter {
    interface View {
        void initViews();
        void showSnackMessage(String snackMessage);
        void showProgress(String title, String caption);
        void hideProgress();
        void showSuccessDialog(String successTitle, String successMessage, Intent adminData);
        void showErrorDialog(String errorTitle, String errorMessage);
        void showExitDialog(String exitTitle, String exitMessage);
    }
    void onStart();
    void onDestroy();
    void submitCredentials(String firstName, String middleName, String lastName, String email, String password);
}
