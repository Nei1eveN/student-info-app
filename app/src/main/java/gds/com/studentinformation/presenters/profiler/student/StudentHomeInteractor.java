package gds.com.studentinformation.presenters.profiler.student;

import android.content.Intent;

import gds.com.studentinformation.models.Student;

public interface StudentHomeInteractor {
    interface StudentDetailsListener {
        void onDetailSuccess(Student student);
        void onDetailFailure(String errorTitle, String errorMessage, Intent errorIntent);
    }

    void getUserDetails(String email, StudentDetailsListener listener);

    interface UserLogoutListener {
        void onLogoutSuccess(String title, String message, Intent intent);
    }

    void userLogout(UserLogoutListener listener);
}
