package gds.com.studentinformation.presenters.profiler.student;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import java.util.Objects;

import gds.com.studentinformation.LoginActivity;
import gds.com.studentinformation.models.Result;
import gds.com.studentinformation.models.Student;
import gds.com.studentinformation.utils.SessionManager;
import gds.com.studentinformation.utils.retrofit.APIClient;
import gds.com.studentinformation.utils.retrofit.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class StudentHomeInteractorImpl implements StudentHomeInteractor {
    private Context context;

    StudentHomeInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUserDetails(String email, StudentDetailsListener listener) {
        APIService detailService = APIClient.getClient(context).create(APIService.class);

        Call<Result> resultCall = detailService.getStudentDetails(email);
        resultCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                if (Objects.requireNonNull(response.body()).getError()) {
                    Intent errorIntent = new Intent(context, LoginActivity.class);
                    listener.onDetailFailure("Error", response.body().getErrorMessage(), errorIntent);
                } else {
                    Result result = response.body();
                    Student student = result.getStudent();
                    listener.onDetailSuccess(student);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                Intent errorIntent = new Intent(context, LoginActivity.class);
                listener.onDetailFailure("Detail Error", t.getMessage(), errorIntent);
            }
        });
    }

    @Override
    public void userLogout(UserLogoutListener listener) {
        SessionManager sessionManager = new SessionManager(context);
        sessionManager.logout();
        Intent intent = new Intent(context, LoginActivity.class);
        listener.onLogoutSuccess("User Logged Out", "You will now go back to Login Screen", intent);
    }
}
