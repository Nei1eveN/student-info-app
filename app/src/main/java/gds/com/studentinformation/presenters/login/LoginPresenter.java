package gds.com.studentinformation.presenters.login;

import android.content.Intent;

import java.util.List;

public interface LoginPresenter {
    interface View {
        void showProgress(String title, String caption);
        void hideProgress();
        void showToastMessage(String message);
        void loadItemsToSpinner(List<String> options);
        void showErrorDialog(String errorTitle, String errorMessage);
        void goToMainActivity(Intent userData);
        void goToStudentRegister();
        void goToAdminRegister();
    }
    void onStart();
    void onDestroy();
    void findStudent();
    void findAdmin();
    void submitCredentials(String email, String password, String selectedUserType);
}
