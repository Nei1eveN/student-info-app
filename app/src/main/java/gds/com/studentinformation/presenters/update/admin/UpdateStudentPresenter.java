package gds.com.studentinformation.presenters.update.admin;

import android.content.Intent;

import java.util.List;

import gds.com.studentinformation.models.Student;

public interface UpdateStudentPresenter {
    interface View {
        void initViews();
        void showProgress(String title, String message);
        void hideProgress();
        void showSuccessDialog(String title, String message, Intent successIntent);
        void showErrorDialog(String title, String message);
        void showExitDialog(String title, String message, Intent errorIntent);
        void setUserInfo(Student student);
        void setProgramOptions(List<String> programs);
        void setYearLevelOptions(List<String> yearLevels);
    }
    void onStart(String studentEmail, String adminEmail);
    void onDestroy();
    void submitUpdateDetails(String studentEmail, String adminEmail, String firstName, String middleName, String lastName,
                             String program, String yearLevel, String birthday, String age, String address);
}
