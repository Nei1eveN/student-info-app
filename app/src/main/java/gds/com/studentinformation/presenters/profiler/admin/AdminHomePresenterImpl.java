package gds.com.studentinformation.presenters.profiler.admin;

import android.content.Context;
import android.content.Intent;

import java.util.List;

import gds.com.studentinformation.models.Admin;
import gds.com.studentinformation.models.Student;

public class AdminHomePresenterImpl implements AdminHomePresenter,
        AdminHomeInteractor.AdminDetailListener, AdminHomeInteractor.ListOfStudentsListener,
        AdminHomeInteractor.RemoveStudentListener, AdminHomeInteractor.UserLogoutListener {
    private AdminHomePresenter.View view;
    private AdminHomeInteractor interactor;

    public AdminHomePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new AdminHomeInteractorImpl(context);
    }

    @Override
    public void onStart(String email) {
        if (view != null) {
            view.initViews();
            view.showProgress("Loading Admin Information", "Please wait...");
//            view.hideProfileViews();
        }
        interactor.getUserDetails(email, this);
        interactor.getStudents(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestUserInfo(String email) {
        if (view != null) {
            view.showProgress("Loading Admin Information", "Please wait...");
//            view.hideProfileViews();
        }
        interactor.getUserDetails(email, this);
        interactor.getStudents(this);
    }

    @Override
    public void requestStudents() {
        interactor.getStudents(this);
    }

    @Override
    public void removeStudent(String studentEmail, Integer studentId) {
        if (view != null) {
            view.showProgress("Removing Student", "Please wait...");
        }
        interactor.deleteStudent(studentEmail, studentId, this);
    }

    @Override
    public void userLogOut() {
        if (view != null) {
            view.showProgress("User Logging Out", "Please wait...");
        }
        interactor.userLogout(this);
    }

    @Override
    public void onDetailSuccess(Admin admin) {
        if (view != null) {
            view.hideProgress();
//            view.showProfileViews();
            view.setUserInfo(admin);
        }
    }

    @Override
    public void onDetailFailure(String errorTitle, String errorMessage, Intent errorIntent) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage, errorIntent);
        }
    }

    @Override
    public void onListSuccess(List<Student> students) {
        if (view != null) {
            view.loadStudents(students);
        }
    }

    @Override
    public void onListFailure(String errorMessage) {
        if (view != null) {
            view.setEmptyState(errorMessage);
        }
    }

    @Override
    public void onRemovalSuccess(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showSuccessDialog(title, message);
        }
    }

    @Override
    public void onRemovalFailure(String title, String message) {
        if (view != null) {
            view.hideProgress();
            view.showSuccessDialog(title, message);
        }
    }

    @Override
    public void onLogoutSuccess(String title, String message, Intent intent) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(title, message, intent);
        }
    }
}
