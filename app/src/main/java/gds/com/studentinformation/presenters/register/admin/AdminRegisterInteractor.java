package gds.com.studentinformation.presenters.register.admin;


import android.content.Intent;

public interface AdminRegisterInteractor {
    interface AdminRegisterListener {
        void onRegisterSuccess(String successTitle, String successMessage, Intent adminData);
        void onRegisterFailure(String errorTitle, String errorMessage);
    }

    void getAdminCredentials(String firstName, String middleName, String lastName, String email, String password, AdminRegisterListener listener);
}
