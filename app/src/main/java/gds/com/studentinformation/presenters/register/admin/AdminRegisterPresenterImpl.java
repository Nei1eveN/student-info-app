package gds.com.studentinformation.presenters.register.admin;

import android.content.Context;
import android.content.Intent;

public class AdminRegisterPresenterImpl implements AdminRegisterPresenter, AdminRegisterInteractor.AdminRegisterListener {
    private AdminRegisterPresenter.View view;
    private AdminRegisterInteractor interactor;

    public AdminRegisterPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new AdminRegisterInteractorImpl(context);
    }

    @Override
    public void onStart() {
        view.initViews();
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void submitCredentials(String firstName, String middleName, String lastName, String email, String password) {
        if (firstName.isEmpty()) {
            view.showErrorDialog("First Name Field Empty", "Please fill in the required field.");
        } else if (middleName.isEmpty()) {
            view.showErrorDialog("Middle Name Field Empty", "Please fill in the required field.\n\nYou may just type N/A if you do not have a Middle Name.");
        } else if (lastName.isEmpty()) {
            view.showErrorDialog("Last Name Field Empty", "Please fill in the required field.");
        } else if (email.isEmpty()) {
            view.showErrorDialog("Email Field Empty", "Please fill in the required field.");
        } else if (password.isEmpty()) {
            view.showErrorDialog("Password Field Empty", "Please fill in the required field.");
        } else if (password.length() < 8) {
            view.showErrorDialog("Insufficient Password Length", "Please create a password with at least 8 characters.");
        } else {
            if (view != null) {
                view.showProgress("Saving Data", "Please wait...");
                interactor.getAdminCredentials(firstName, middleName, lastName, email, password, this);
            }
        }
    }

    @Override
    public void onRegisterSuccess(String successTitle, String successMessage, Intent adminData) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(successTitle, successMessage);
        }
    }

    @Override
    public void onRegisterFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }
}
