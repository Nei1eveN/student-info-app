package gds.com.studentinformation.presenters.update.student;

import android.content.Context;
import android.content.Intent;

import java.util.List;

import gds.com.studentinformation.models.Student;

public class StudentProfileEditPresenterImpl implements StudentProfileEditPresenter, StudentProfileEditInteractor.StudentProfileListener, StudentProfileEditInteractor.ProgramSelectionListener, StudentProfileEditInteractor.YearLevelSelectionListener, StudentProfileEditInteractor.UpdateStudentListener {
    private View view;
    private StudentProfileEditInteractor interactor;

    public StudentProfileEditPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentProfileEditInteractorImpl(context);
    }

    @Override
    public void onStart(String studentEmail) {
        if (view != null) {
            view.initViews();
            view.showProgress("Loading Details", "Please wait...");
        }
        interactor.getStudentDetail(studentEmail, this);
        interactor.getProgramSelection(studentEmail, this);
        interactor.getYearLevelSelection(studentEmail, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void submitUpdateDetails(String studentEmail,
                                    String firstName, String middleName, String lastName,
                                    String program, String yearLevel,
                                    String birthday, String age, String address) {
        if (firstName.isEmpty()) {
            view.showErrorDialog("First Name Field Empty", "At least write something into it.");
        } else if (middleName.isEmpty()) {
            view.showErrorDialog("Middle Name Field Empty", "If the student has no middle name, just write N/A.");
        } else if (lastName.isEmpty()) {
            view.showErrorDialog("Last Name Field Empty", "At least write something into it.");
        } else if (program.isEmpty()) {
            view.showErrorDialog("Program Field Empty", "Please select a program");
        } else if (yearLevel.isEmpty()) {
            view.showErrorDialog("Year Level Field Empty", "Please select a program");
        } else if (birthday.isEmpty()) {
            view.showErrorDialog("Birthday Field Empty", "At least write something into it.");
        } else if (age.isEmpty()) {
            view.showErrorDialog("Age Field Empty", "At least write something into it.");
        } else if (address.isEmpty()) {
            view.showErrorDialog("Address Field Empty", "At least write something into it.");
        } else {
            if (view != null) {
                view.showProgress("Saving Changes", "Please wait...");
            }
            interactor.getUpdateDetails
                    (studentEmail, firstName, middleName, lastName,
                            program, yearLevel, birthday, age, address, this);
        }
    }

    @Override
    public void onUpdateSuccess(String successTitle, String successMessage, Intent intent) {
        if (view != null) {
            view.hideProgress();
            view.showSuccessDialog(successTitle, successMessage, intent);
        }
    }

    @Override
    public void onUpdateFailure(String errorTitle, String errorMessage, Intent intent) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(errorTitle, errorMessage, intent);
        }
    }

    @Override
    public void onProfileSuccess(Student student) {
        if (view != null) {
            view.hideProgress();
            view.setUserInfo(student);
        }
    }

    @Override
    public void onProfileFailure(String errorTitle, String errorMessage, Intent intent) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(errorTitle, errorMessage, intent);
        }
    }

    @Override
    public void onProgramSelection(List<String> programs) {
        if (view != null) {
            view.hideProgress();
            view.setProgramOptions(programs);
        }
    }

    @Override
    public void onProgramSelectionFailure(String errorTitle, String errorMessage, Intent intent) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(errorTitle, errorMessage, intent);
        }
    }

    @Override
    public void onYearLevelSelection(List<String> yearLevels) {
        if (view != null) {
            view.hideProgress();
            view.setYearLevelOptions(yearLevels);
        }
    }

    @Override
    public void onYearLevelSelectionFailure(String errorTitle, String errorMessage, Intent intent) {
        if (view != null) {
            view.hideProgress();
            view.showExitDialog(errorTitle, errorMessage, intent);
        }
    }
}
