package gds.com.studentinformation.presenters.register.admin;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.Date;
import java.util.Objects;

import gds.com.studentinformation.AdminProfileActivity;
import gds.com.studentinformation.models.Admin;
import gds.com.studentinformation.models.Result;
import gds.com.studentinformation.utils.retrofit.APIClient;
import gds.com.studentinformation.utils.retrofit.APIService;
import gds.com.studentinformation.utils.SimpleCrypto;
import gds.com.studentinformation.utils.sqlite.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static gds.com.studentinformation.utils.Constants.ADMIN_TABLE_NAME;
import static gds.com.studentinformation.utils.Constants.COLUMN_EMAIL;
import static gds.com.studentinformation.utils.Constants.COLUMN_ENCRYPTED_PASSWORD;
import static gds.com.studentinformation.utils.Constants.COLUMN_FIRSTNAME;
import static gds.com.studentinformation.utils.Constants.COLUMN_LASTNAME;
import static gds.com.studentinformation.utils.Constants.COLUMN_MIDDLENAME;
import static gds.com.studentinformation.utils.Constants.COLUMN_PASSWORD;
import static gds.com.studentinformation.utils.Constants.COLUMN_REGISTRATION_TIMESTAMP;
import static gds.com.studentinformation.utils.Constants.COLUMN_USER_ROLE;
import static gds.com.studentinformation.utils.Constants.USER_TABLE_NAME;
import static gds.com.studentinformation.utils.Constants.isNetworkAvailable;
import static gds.com.studentinformation.utils.Constants.registrationTimeFormat;

class AdminRegisterInteractorImpl implements AdminRegisterInteractor {
    private Context context;

    AdminRegisterInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getAdminCredentials(String firstName, String middleName, String lastName, String email, String password, AdminRegisterListener listener) {

        if (!isNetworkAvailable(context)) {
            listener.onRegisterFailure("Network Error", "Please check your internet connection");
        } else {
            APIService registerService = APIClient.getClient(context).create(APIService.class);

            Call<Result> adminCall = registerService.saveAdminCredentials(firstName, middleName, lastName, email, registrationTimeFormat.format(new Date()));
            adminCall.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                    if (Objects.requireNonNull(response.body()).getError()) {
                        listener.onRegisterFailure("Existing Email", response.body().getErrorMessage());
                    } else {
                        Result result = response.body();
                        Call<Result> userCall = registerService.saveUserCredentials(email, password, password, "Admin");
                        userCall.enqueue(new Callback<Result>() {
                            @Override
                            public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                                DatabaseHelper databaseHelper = new DatabaseHelper(context);
                                SQLiteDatabase database = databaseHelper.getWritableDatabase();

                                database.beginTransaction();
                                try {
                                    ContentValues adminValues = new ContentValues();
                                    adminValues.put(COLUMN_FIRSTNAME, firstName);
                                    adminValues.put(COLUMN_MIDDLENAME, middleName);
                                    adminValues.put(COLUMN_LASTNAME, lastName);
                                    adminValues.put(COLUMN_EMAIL, email);
                                    adminValues.put(COLUMN_REGISTRATION_TIMESTAMP, registrationTimeFormat.format(new Date()));

                                    ContentValues userValues = new ContentValues();
                                    userValues.put(COLUMN_EMAIL, email);
                                    userValues.put(COLUMN_PASSWORD, password);
                                    userValues.put(COLUMN_ENCRYPTED_PASSWORD, new SimpleCrypto().encrypt("encrypted_password", password));
                                    userValues.put(COLUMN_USER_ROLE, "Admin");

                                    database.insertWithOnConflict(USER_TABLE_NAME, null, userValues, SQLiteDatabase.CONFLICT_NONE);
                                    database.insertWithOnConflict(ADMIN_TABLE_NAME, null, adminValues, SQLiteDatabase.CONFLICT_NONE);
                                    database.setTransactionSuccessful();
                                } finally {
                                    database.endTransaction();
                                }
                                Admin admin = result.getAdmin();
                                Intent adminData = new Intent(context, AdminProfileActivity.class);
                                adminData.putExtra(COLUMN_EMAIL, admin.getEmail());

                                listener.onRegisterSuccess("Admin Registered", "Registration Successful", adminData);
                            }

                            @Override
                            public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                                listener.onRegisterFailure("Saving Error", t.getMessage());
                            }
                        });
                    }

                }

                @Override
                public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                    listener.onRegisterFailure("Saving Error", t.getMessage());
                }
            });
        }
    }
}
