package gds.com.studentinformation.presenters.login;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import gds.com.studentinformation.AdminProfileActivity;
import gds.com.studentinformation.StudentHomeActivity;
import gds.com.studentinformation.models.Result;
import gds.com.studentinformation.models.User;
import gds.com.studentinformation.utils.SessionManager;
import gds.com.studentinformation.utils.retrofit.APIClient;
import gds.com.studentinformation.utils.retrofit.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static gds.com.studentinformation.utils.Constants.COLUMN_EMAIL;
import static gds.com.studentinformation.utils.Constants.isNetworkAvailable;

class LoginInteractorImpl implements LoginInteractor {
    private Context context;

    LoginInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUser(FindUserListener listener) {
        SessionManager sessionManager = new SessionManager(context);
        if (sessionManager.isLoggedIn()) {
            Intent userData;
            if (sessionManager.getUserRole().equals("Student")) {
                userData = new Intent(context, StudentHomeActivity.class);
                userData.putExtra(COLUMN_EMAIL, sessionManager.getUserEmail());
                listener.onUserFound("Logged in as "+sessionManager.getUserEmail(), userData);
                } else {
                userData = new Intent(context, AdminProfileActivity.class);
                userData.putExtra(COLUMN_EMAIL, sessionManager.getUserEmail());
                listener.onUserFound("Logged in as "+sessionManager.getUserEmail(), userData);
                }
        } else {
            List<String> loginAsOptions = new ArrayList<>();
            loginAsOptions.add("Student");
            loginAsOptions.add("Admin");
            listener.onUserEmpty(loginAsOptions);
        }
    }

    @Override
    public void checkAdminIfExisting(FindAdminListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onAdminFound("Network Error", "Please check your internet connection.");
        } else {
            APIService adminCheckService = APIClient.getClient(context).create(APIService.class);

            Call<Result> resultCall = adminCheckService.checkIfThereIsAnAdmin();
            resultCall.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                    if (Objects.requireNonNull(response.body()).getError()) {
                        listener.onAdminFound("Admin Found", response.body().getErrorMessage());
                    } else {
                        listener.onAdminNotFound();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                    listener.onAdminFound("Checking Error", t.getMessage());
                }
            });
        }
    }

    @Override
    public void getLoginCredentials(String email, String password, String selectedUserType, LoginCredentialsListener listener) {
        if (!isNetworkAvailable(context)) {
            listener.onLoginFailure("Network Error", "Please check your internet connection.");
        } else {
            APIService loginService = APIClient.getClient(context).create(APIService.class);

            Call<Result> userCall = loginService.getLoginCredentials(email, password, selectedUserType);
            userCall.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                    if (Objects.requireNonNull(response.body()).getError()) {
                        listener.onLoginFailure("Login Error", Objects.requireNonNull(response.body()).getErrorMessage());
                    } else {
                        Result result = response.body();
                        User user = result.getUser();

                        SessionManager sessionManager = new SessionManager(context);

                        sessionManager.setLogin(true, user.getEmail(), user.getRole());

                        Log.d("userEmail", user.getEmail());
                        Intent userData;
                        if (user.getRole().equals("Student")) {
                            userData = new Intent(context, StudentHomeActivity.class);
                            userData.putExtra(COLUMN_EMAIL, user.getEmail());
                        } else {
                            userData = new Intent(context, AdminProfileActivity.class);
                            userData.putExtra(COLUMN_EMAIL, user.getEmail());
                        }
                        listener.onLoginSuccess("Login Successfully", userData);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                    listener.onLoginFailure("Login Error", t.getMessage());
                }
            });
        }
    }
}
