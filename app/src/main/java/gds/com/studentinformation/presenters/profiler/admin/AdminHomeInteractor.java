package gds.com.studentinformation.presenters.profiler.admin;

import android.content.Intent;

import java.util.List;

import gds.com.studentinformation.models.Admin;
import gds.com.studentinformation.models.Student;

public interface AdminHomeInteractor {
    interface AdminDetailListener {
        void onDetailSuccess(Admin admin);

        void onDetailFailure(String errorTitle, String errorMessage, Intent errorIntent);
    }

    void getUserDetails(String email, AdminDetailListener listener);

    interface ListOfStudentsListener {
        void onListSuccess(List<Student> students);

        void onListFailure(String errorMessage);
    }

    void getStudents(ListOfStudentsListener listener);

    interface RemoveStudentListener {
        void onRemovalSuccess(String title, String message);
        void onRemovalFailure(String title, String message);
    }

    void deleteStudent(String email, Integer studentId, RemoveStudentListener listener);

    interface UserLogoutListener {
        void onLogoutSuccess(String title, String message, Intent intent);
    }

    void userLogout(UserLogoutListener listener);
}
