package gds.com.studentinformation.presenters.profiler.student;

import android.content.Intent;

import gds.com.studentinformation.models.Student;

public interface StudentHomePresenter {
    interface View {
        void initViews();
        void showViews();
        void hideViews();
        void showProgress(String title, String caption);
        void hideProgress();
        void showErrorDialog(String title, String message, Intent intent);
        void setUserInfo(Student student);
    }
    void onStart(String email);
    void onDestroy();
    void requestUserInfo(String email);
    void userLogOut();
}
