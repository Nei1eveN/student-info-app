package gds.com.studentinformation.presenters.profiler.student;

import android.content.Context;
import android.content.Intent;

import gds.com.studentinformation.models.Student;

public class StudentHomePresenterImpl implements StudentHomePresenter, StudentHomeInteractor.StudentDetailsListener, StudentHomeInteractor.UserLogoutListener {
    private StudentHomePresenter.View view;
    private StudentHomeInteractor interactor;

    public StudentHomePresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new StudentHomeInteractorImpl(context);
    }

    @Override
    public void onStart(String email) {
        if (view != null) {
            view.initViews();
            view.showProgress("Loading Student Information", "Please wait...");
//            view.hideViews();
        }
        interactor.getUserDetails(email, this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void requestUserInfo(String email) {
        if (view != null) {
            view.showProgress("Loading Student Information", "Please wait...");
//            view.hideViews();
        }
        interactor.getUserDetails(email, this);
    }

    @Override
    public void userLogOut() {
        if (view != null) {
            view.showProgress("User Logging Out", "Please wait...");
        }
        interactor.userLogout(this);
    }

    @Override
    public void onDetailSuccess(Student student) {
        if (view != null) {
            view.hideProgress();
//            view.showViews();
            view.setUserInfo(student);
        }
    }

    @Override
    public void onDetailFailure(String errorTitle, String errorMessage, Intent errorIntent) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage, errorIntent);
        }
    }

    @Override
    public void onLogoutSuccess(String title, String message, Intent intent) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(title, message, intent);
        }
    }
}
