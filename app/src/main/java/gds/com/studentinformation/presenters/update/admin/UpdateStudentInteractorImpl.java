package gds.com.studentinformation.presenters.update.admin;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import gds.com.studentinformation.AdminProfileActivity;
import gds.com.studentinformation.models.Result;
import gds.com.studentinformation.models.Student;
import gds.com.studentinformation.utils.retrofit.APIClient;
import gds.com.studentinformation.utils.retrofit.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static gds.com.studentinformation.utils.Constants.COLUMN_EMAIL;

class UpdateStudentInteractorImpl implements UpdateStudentInteractor {
    private Context context;

    UpdateStudentInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUpdateDetails(String studentEmail, String adminEmail, String firstName, String middleName, String lastName, String program, String yearLevel, String birthday, String age, String address, UpdateStudentListener listener) {
        APIService updateService = APIClient.getClient(context).create(APIService.class);

        Call<Result> studentCall = updateService.getStudentDetails(studentEmail);
        studentCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                if (Objects.requireNonNull(response.body()).getError()) {
                    Intent errorIntent = new Intent(context, AdminProfileActivity.class);
                    errorIntent.putExtra(COLUMN_EMAIL, adminEmail);
                    listener.onUpdateFailure("Error", response.body().getErrorMessage(), errorIntent);
                } else {
                    Result result = response.body();
                    Student student = result.getStudent();
                    Call<Result> resultCall = updateService.updateStudent(firstName, middleName, lastName, program, yearLevel, birthday, age
                            , address, student.getStudentId());
                    resultCall.enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                            if (Objects.requireNonNull(response.body()).getError()) {
                                Intent errorIntent = new Intent(context, AdminProfileActivity.class);
                                errorIntent.putExtra(COLUMN_EMAIL, adminEmail);
                                listener.onUpdateFailure("Error", response.body().getErrorMessage(), errorIntent);
                            } else {
                                Intent successIntent = new Intent(context, AdminProfileActivity.class);
                                successIntent.putExtra(COLUMN_EMAIL, adminEmail);
                                listener.onUpdateSuccess("Student Updated", "Preference for Student has been changed", successIntent);
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                            Intent errorIntent = new Intent(context, AdminProfileActivity.class);
                            errorIntent.putExtra(COLUMN_EMAIL, adminEmail);
                            listener.onUpdateFailure("Update Error", t.getMessage(), errorIntent);
                        }
                    });
                }
            }

            @Override
            public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                Intent errorIntent = new Intent(context, AdminProfileActivity.class);
                errorIntent.putExtra(COLUMN_EMAIL, adminEmail);
                listener.onUpdateFailure("Student Detail Error", t.getMessage(), errorIntent);
            }
        });


    }

    @Override
    public void getStudentDetail(String studentEmail, String adminEmail, StudentProfileListener listener) {
        APIService detailService = APIClient.getClient(context).create(APIService.class);

        Call<Result> resultCall = detailService.getStudentDetails(studentEmail);
        resultCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                if (Objects.requireNonNull(response.body()).getError()) {
                    Intent errorIntent = new Intent(context, AdminProfileActivity.class);
                    errorIntent.putExtra(COLUMN_EMAIL, adminEmail);
                    listener.onProfileFailure("Error", response.body().getErrorMessage(), errorIntent);
                } else {
                    Result result = response.body();
                    Student student = result.getStudent();
                    listener.onProfileSuccess(student);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                Intent errorIntent = new Intent(context, AdminProfileActivity.class);
                errorIntent.putExtra(COLUMN_EMAIL, adminEmail);
                listener.onProfileFailure("Detail Error", t.getMessage(), errorIntent);
            }
        });
    }

    @Override
    public void getProgramSelection(String studentEmail, String adminEmail, ProgramSelectionListener listener) {
        APIService detailService = APIClient.getClient(context).create(APIService.class);

        Call<Result> resultCall = detailService.getStudentDetails(studentEmail);
        resultCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                if (Objects.requireNonNull(response.body()).getError()) {
                    Intent errorIntent = new Intent(context, AdminProfileActivity.class);
                    errorIntent.putExtra(COLUMN_EMAIL, adminEmail);
                    listener.onProgramSelectionFailure("Error", response.body().getErrorMessage(), errorIntent);
                } else {
                    Result result = response.body();
                    Student student = result.getStudent();

                    List<String> programs = new ArrayList<>();

                    if ("Associate in Computer Technology".equals(student.getProgram())) {
                        programs.add("Associate in Computer Technology");
                        programs.add("Computer Science");
                        programs.add("Information Systems");
                        programs.add("Information Technology");

                    } else if ("Computer Science".equals(student.getProgram())) {
                        programs.add("Computer Science");
                        programs.add("Associate in Computer Technology");
                        programs.add("Information Systems");
                        programs.add("Information Technology");

                    } else if ("Information Systems".equals(student.getProgram())) {
                        programs.add("Information Systems");
                        programs.add("Associate in Computer Technology");
                        programs.add("Computer Science");
                        programs.add("Information Technology");

                    } else if ("Information Technology".equals(student.getProgram())){
                        programs.add("Information Technology");
                        programs.add("Associate in Computer Technology");
                        programs.add("Computer Science");
                        programs.add("Information Systems");
                    } else {
                        programs.add("Associate in Computer Technology");
                        programs.add("Computer Science");
                        programs.add("Information Systems");
                        programs.add("Information Technology");
                    }

                    listener.onProgramSelection(programs);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                Intent errorIntent = new Intent(context, AdminProfileActivity.class);
                errorIntent.putExtra(COLUMN_EMAIL, adminEmail);
                listener.onProgramSelectionFailure("Detail Error", t.getMessage(), errorIntent);
            }
        });
    }

    @Override
    public void getYearLevelSelection(String studentEmail, String adminEmail, YearLevelSelectionListener listener) {
        APIService detailService = APIClient.getClient(context).create(APIService.class);

        Call<Result> resultCall = detailService.getStudentDetails(studentEmail);
        resultCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                if (Objects.requireNonNull(response.body()).getError()) {
                    Intent errorIntent = new Intent(context, AdminProfileActivity.class);
                    errorIntent.putExtra(COLUMN_EMAIL, adminEmail);
                    listener.onYearLevelSelectionFailure("Error", response.body().getErrorMessage(), errorIntent);
                } else {
                    Result result = response.body();
                    Student student = result.getStudent();

                    List<String> yearLevels = new ArrayList<>();

                    if ("1st".equals(student.getYearLevel())) {
                        yearLevels.add("1st");
                        yearLevels.add("2nd");
                        yearLevels.add("3rd");
                        yearLevels.add("4th");
                    } else if ("2nd".equals(student.getYearLevel())) {
                        yearLevels.add("2nd");
                        yearLevels.add("1st");
                        yearLevels.add("3rd");
                        yearLevels.add("4th");
                    } else if ("3rd".equals(student.getYearLevel())) {
                        yearLevels.add("3rd");
                        yearLevels.add("1st");
                        yearLevels.add("2nd");
                        yearLevels.add("4th");
                    } else if ("4th".equals(student.getYearLevel())){
                        yearLevels.add("4th");
                        yearLevels.add("1st");
                        yearLevels.add("2nd");
                        yearLevels.add("3rd");
                    } else {
                        yearLevels.add("1st");
                        yearLevels.add("2nd");
                        yearLevels.add("3rd");
                        yearLevels.add("4th");
                    }

                    listener.onYearLevelSelection(yearLevels);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                Intent errorIntent = new Intent(context, AdminProfileActivity.class);
                errorIntent.putExtra(COLUMN_EMAIL, adminEmail);
                listener.onYearLevelSelectionFailure("Detail Error", t.getMessage(), errorIntent);
            }
        });
    }
}
