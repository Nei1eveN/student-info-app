package gds.com.studentinformation.presenters.profiler.admin;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import java.util.List;
import java.util.Objects;

import gds.com.studentinformation.LoginActivity;
import gds.com.studentinformation.models.Admin;
import gds.com.studentinformation.models.Result;
import gds.com.studentinformation.models.Student;
import gds.com.studentinformation.utils.SessionManager;
import gds.com.studentinformation.utils.retrofit.APIClient;
import gds.com.studentinformation.utils.retrofit.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class AdminHomeInteractorImpl implements AdminHomeInteractor {
    private Context context;

    AdminHomeInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getUserDetails(String email, AdminDetailListener listener) {
        APIService detailService = APIClient.getClient(context).create(APIService.class);
        Call<Result> userCall = detailService.getAdminDetails(email);
        userCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                if (Objects.requireNonNull(response.body()).getError()) {
                    Intent errorIntent = new Intent(context, LoginActivity.class);
                    listener.onDetailFailure("User Detail Error", response.body().getErrorMessage(), errorIntent);
                } else {
                    Result result = response.body();
                    Admin admin = result.getAdmin();
                    listener.onDetailSuccess(admin);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                Intent errorIntent = new Intent(context, LoginActivity.class);
                listener.onDetailFailure("Detail Error", t.getMessage(), errorIntent);
            }
        });
    }

    @Override
    public void getStudents(ListOfStudentsListener listener) {
        APIService detailService = APIClient.getClient(context).create(APIService.class);
        Call<Result> resultCall = detailService.getAllStudents();
        resultCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                if (Objects.requireNonNull(response.body()).getError()) {
                    listener.onListFailure(response.body().getErrorMessage());
                } else {
                    Result result = response.body();
                    List<Student> students = result.getStudents();
                    listener.onListSuccess(students);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                listener.onListFailure(t.getMessage());
            }
        });
    }

    @Override
    public void deleteStudent(String email, Integer studentId, RemoveStudentListener listener) {
        APIService deleteService = APIClient.getClient(context).create(APIService.class);
        Call<Result> deleteCall = deleteService.deleteStudent(email, studentId);
        deleteCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                if (Objects.requireNonNull(response.body()).getError()) {
                    listener.onRemovalFailure("Removal Error", response.body().getErrorMessage());
                } else {
                    listener.onRemovalSuccess("Student Removed", "Student Removed Successfully.");
                }
            }

            @Override
            public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
                listener.onRemovalFailure("Unknown Error", t.getMessage());
            }
        });
    }

    @Override
    public void userLogout(UserLogoutListener listener) {
        SessionManager sessionManager = new SessionManager(context);
        sessionManager.logout();
        Intent intent = new Intent(context, LoginActivity.class);
        listener.onLogoutSuccess("User Logged Out", "You will now go back to Login Screen", intent);
    }
}
