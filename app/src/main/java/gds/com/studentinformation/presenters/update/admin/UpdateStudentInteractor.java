package gds.com.studentinformation.presenters.update.admin;

import android.content.Intent;

import java.util.List;

import gds.com.studentinformation.models.Student;

public interface UpdateStudentInteractor {
    interface UpdateStudentListener {
        void onUpdateSuccess(String successTitle, String successMessage, Intent intent);
        void onUpdateFailure(String errorTitle, String errorMessage, Intent intent);
    }

    void getUpdateDetails(String studentEmail, String adminEmail, String firstName, String middleName, String lastName,
                          String program, String yearLevel, String birthday, String age, String address, UpdateStudentListener listener);

    interface StudentProfileListener {
        void onProfileSuccess(Student student);
        void onProfileFailure(String errorTitle, String errorMessage, Intent intent);
    }

    void getStudentDetail(String studentEmail, String adminEmail, StudentProfileListener listener);

    interface ProgramSelectionListener {
        void onProgramSelection(List<String> programs);
        void onProgramSelectionFailure(String errorTitle, String errorMessage, Intent intent);
    }

    void getProgramSelection(String email, String adminEmail, ProgramSelectionListener listener);

    interface YearLevelSelectionListener {
        void onYearLevelSelection(List<String> yearLevels);
        void onYearLevelSelectionFailure(String errorTitle, String errorMessage, Intent intent);
    }

    void getYearLevelSelection(String email, String adminEmail, YearLevelSelectionListener listener);
}
