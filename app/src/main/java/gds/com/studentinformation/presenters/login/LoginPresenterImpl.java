package gds.com.studentinformation.presenters.login;

import android.content.Context;
import android.content.Intent;

import java.util.List;

public class LoginPresenterImpl implements LoginPresenter, LoginInteractor.FindUserListener, LoginInteractor.LoginCredentialsListener, LoginInteractor.FindAdminListener {
    private LoginPresenter.View view;
    private LoginInteractor interactor;

    public LoginPresenterImpl(View view, Context context) {
        this.view = view;
        this.interactor = new LoginInteractorImpl(context);
    }

    @Override
    public void onStart() {
        interactor.getUser(this);
    }

    @Override
    public void onDestroy() {
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void findStudent() {
        if (view != null) {
            view.goToStudentRegister();
        }
    }

    @Override
    public void findAdmin() {
        if (view != null) {
            view.showProgress("Checking Admin", "Please wait...");
            interactor.checkAdminIfExisting(this);
        }
    }

    @Override
    public void submitCredentials(String email, String password, String selectedUserType) {
        if (email.isEmpty()) {
            view.showErrorDialog("Email Field Empty", "Please fill in the required field.");
        } else if (password.isEmpty()) {
            view.showErrorDialog("Password Field Empty", "Please fill in the required field.");
        } else {
            if (view != null) {
                view.showProgress("Submitting Credentials", "Please wait...");
                interactor.getLoginCredentials(email, password, selectedUserType, this);
            }
        }
    }

    @Override
    public void onUserFound(String successMessage, Intent userData) {
        if (view != null) {
            view.showToastMessage(successMessage);
            view.goToMainActivity(userData);
        }
    }

    @Override
    public void onUserEmpty(List<String> loginAsOptions) {
        if (view != null) {
            view.loadItemsToSpinner(loginAsOptions);
        }
    }

    @Override
    public void onLoginSuccess(String successMessage, Intent userData) {
        if (view != null) {
            view.hideProgress();
            view.showToastMessage(successMessage);
            view.goToMainActivity(userData);
        }
    }

    @Override
    public void onLoginFailure(String errorTitle, String errorMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(errorTitle, errorMessage);
        }
    }

    @Override
    public void onAdminFound(String successTitle, String successMessage) {
        if (view != null) {
            view.hideProgress();
            view.showErrorDialog(successTitle, successMessage);
        }
    }

    @Override
    public void onAdminNotFound() {
        if (view != null) {
            view.hideProgress();
            view.goToAdminRegister();
        }
    }
}
