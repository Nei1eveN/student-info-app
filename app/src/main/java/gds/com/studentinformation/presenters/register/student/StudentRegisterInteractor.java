package gds.com.studentinformation.presenters.register.student;

import android.content.Intent;

import java.util.List;

public interface StudentRegisterInteractor {
    interface StudentRegisterListener {
        void onRegisterSuccess(String successTitle, String successMessage, Intent studentData);
        void onRegisterFailure(String errorTitle, String errorMessage);
    }

    interface ProgramListener {
        void onProgramsLoaded(List<String> programs);
    }

    interface YearLevelsListener {
        void onYearLevelsLoaded(List<String> yearLevels);
    }

    void getProgramOptions(ProgramListener listener);

    void getYearLevelOptions(YearLevelsListener listener);

    void getStudentCredentials(String firstName, String middleName, String lastName,
                               String program, String yearLevel, String email,
                               String birthday, String age, String address, String password, StudentRegisterListener listener);
}
