package gds.com.studentinformation.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import gds.com.studentinformation.R;
import gds.com.studentinformation.adapters.listener.StudentItemClickListener;
import gds.com.studentinformation.models.Student;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentViewHolder>{
    private Context context;
    private List<Student> students;
    private StudentItemClickListener clickListener;

    public StudentAdapter(Context context, List<Student> students, StudentItemClickListener listener) {
        this.context = context;
        this.students = students;
        this.clickListener = listener;
    }

    class StudentViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout constraintLayout;
        TextView studentName, studentProgram, studentYearLevel;

        StudentViewHolder(@NonNull View itemView) {
            super(itemView);

            constraintLayout = itemView.findViewById(R.id.constraintDetail);
            studentName = itemView.findViewById(R.id.tvStudentName);
            studentProgram = itemView.findViewById(R.id.tvStudentProgram);
            studentYearLevel = itemView.findViewById(R.id.tvStudentYearLevel);
        }
    }

    @NonNull
    @Override
    public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.student_item, viewGroup, false);
        return new StudentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentViewHolder holder, int i) {
        Student student = students.get(i);

        holder.studentName.setText(String.format("%s, %s %s", student.getLastName(), student.getFirstName(), student.getMiddleName()));
        holder.studentProgram.setText(student.getProgram());
        holder.studentYearLevel.setText(student.getYearLevel());

        holder.constraintLayout.setOnClickListener(v -> clickListener.onClick(student));
    }

    @Override
    public int getItemCount() {
        return students.size();
    }
}
