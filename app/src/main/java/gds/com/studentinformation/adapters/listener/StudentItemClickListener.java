package gds.com.studentinformation.adapters.listener;

import gds.com.studentinformation.models.Student;

public interface StudentItemClickListener {
    void onClick(Student student);
}
