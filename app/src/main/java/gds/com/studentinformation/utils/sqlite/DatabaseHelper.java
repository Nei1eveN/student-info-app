package gds.com.studentinformation.utils.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import gds.com.studentinformation.utils.Constants;

import static gds.com.studentinformation.utils.Constants.DATABASE_NAME;
import static gds.com.studentinformation.utils.Constants.DATABASE_VERSION;

public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constants.CREATE_ADMIN_TABLE);
        db.execSQL(Constants.CREATE_USER_TABLE);
        db.execSQL(Constants.CREATE_STUDENT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Constants.ADMIN_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.USER_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.STUDENT_TABLE_NAME);
        onCreate(db);
    }
}
