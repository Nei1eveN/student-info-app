package gds.com.studentinformation.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import gds.com.studentinformation.LoginActivity;
import gds.com.studentinformation.models.User;

import static gds.com.studentinformation.utils.Constants.COLUMN_EMAIL;
import static gds.com.studentinformation.utils.Constants.COLUMN_USER_ROLE;
import static gds.com.studentinformation.utils.Constants.KEY_IS_LOGGEDIN;
import static gds.com.studentinformation.utils.Constants.KEY_USER_EMAIL;
import static gds.com.studentinformation.utils.Constants.KEY_USER_ROLE;
import static gds.com.studentinformation.utils.Constants.PREF_NAME;
import static gds.com.studentinformation.utils.Constants.PRIVATE_MODE;

public class SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    private SharedPreferences pref;

    private SharedPreferences.Editor editor;
    private Context _context;

    public SessionManager(Context _context) {
        this._context = _context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn, String email, String user_role) {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        editor.putString(COLUMN_EMAIL, email);
        editor.putString(COLUMN_USER_ROLE, user_role);
        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified.");
    }

    public String getUserRole() {
        return pref.getString(COLUMN_USER_ROLE, "");
    }

    public String getUserEmail() {
        return pref.getString(COLUMN_EMAIL, "");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    //Logout user
    public void logout() {
        editor.clear();
        editor.commit();
    }
}
