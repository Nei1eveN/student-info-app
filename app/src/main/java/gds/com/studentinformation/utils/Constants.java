package gds.com.studentinformation.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class Constants {

    //Splash Seconds
    public static int TWO_SECONDS = 2000;

    // Shared pref mode
    static int PRIVATE_MODE = 0;

    // Shared preferences file name
    static final String PREF_NAME = "UserLogin";

    static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    static final String KEY_USER_EMAIL = "email";
    static final String KEY_USER_ROLE = "user_role";

    //Register Options
    public static final CharSequence[] REGISTER_AS_OPTIONS = {"Student", "Admin"};

    // Database Version
    public static final int DATABASE_VERSION = 7;
    // Database Name
    public static final String DATABASE_NAME = "info_app";

    //Student SELECT WHERE Query

    //Student CREATE_TABLE Query
    public static final String STUDENT_TABLE_NAME = "Student";

    public static final String COLUMN_STUDENT_ID = "studentId";
    public static final String COLUMN_FIRSTNAME = "firstName";
    public static final String COLUMN_MIDDLENAME = "middleName";
    public static final String COLUMN_LASTNAME = "lastName";
    public static final String COLUMN_PROGRAM = "program";
    public static final String COLUMN_YEAR_LEVEL = "yearLevel";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_BIRTHDAY = "birthday";
    public static final String COLUMN_AGE = "age";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_REGISTRATION_TIMESTAMP = "registrationTimeStamp";

    public static final String CREATE_STUDENT_TABLE =
            "CREATE TABLE "+STUDENT_TABLE_NAME+"("+ COLUMN_STUDENT_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
                    COLUMN_FIRSTNAME+" TEXT,"+COLUMN_MIDDLENAME+" TEXT,"+COLUMN_LASTNAME+" TEXT,"+
                    COLUMN_PROGRAM+" TEXT,"+COLUMN_YEAR_LEVEL+" TEXT,"+COLUMN_EMAIL+" TEXT,"+
                    COLUMN_BIRTHDAY+" TEXT,"+COLUMN_AGE+" TEXT,"+COLUMN_ADDRESS+" TEXT,"+
                    COLUMN_REGISTRATION_TIMESTAMP+" TEXT"+
                    ")";

    //Admin CREATE_TABLE Query
    public static final String ADMIN_TABLE_NAME = "Admins";

    public static final String COLUMN_ADMIN_ID = "adminId";

    public static final String CREATE_ADMIN_TABLE =
            "CREATE TABLE "+ADMIN_TABLE_NAME+"("+ COLUMN_ADMIN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
                    COLUMN_FIRSTNAME+" TEXT,"+COLUMN_MIDDLENAME+" TEXT,"+COLUMN_LASTNAME+" TEXT,"+
                    COLUMN_EMAIL+" TEXT,"+COLUMN_REGISTRATION_TIMESTAMP+" TEXT"+
                    ")";

    //User CREATE_TABLE Query
    public static final String USER_TABLE_NAME = "Users";

    public static final String COLUMN_USER_ID = "userId";

    public static final String COLUMN_PASSWORD = "password";

    public static final String COLUMN_ENCRYPTED_PASSWORD = "encrypted_password";

    public static final String COLUMN_USER_ROLE = "user_role";

    public static final String CREATE_USER_TABLE =
            "CREATE TABLE "+USER_TABLE_NAME+"("+ COLUMN_USER_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
                    COLUMN_EMAIL+" TEXT,"+COLUMN_PASSWORD+" TEXT,"+COLUMN_ENCRYPTED_PASSWORD+" TEXT,"+COLUMN_USER_ROLE+" TEXT"+
                    ")";

    //SimpleDateFormats
    public static final SimpleDateFormat registrationTimeFormat = new SimpleDateFormat("EEE, MMM d, yyyy h:mm a", Locale.ENGLISH);

    //REQUEST CODE
    public static int REQUEST_CODE = 0;

    //Cache Size
    public static int cacheSize = 10 * 1024 * 1024;

    //Check Network Availability
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    //URL for database
    public static String openweathermap_url = "http://api.openweathermap.org/";

    public static String webhost_url = "http://gdstrainingian.x10host.com";
//    public static String webhost_url = "https://student-info-app.000webhostapp.com/";

    public static String webhost_registration_url = "http://gdstrainingian.x10host.com/register.php";

    public static void showExitDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Do you want to exit?");
        builder.setPositiveButton("yes", (dialogInterface, i) -> {
            ((Activity) context).finishAffinity();
        });
        builder.setNegativeButton("no", null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
