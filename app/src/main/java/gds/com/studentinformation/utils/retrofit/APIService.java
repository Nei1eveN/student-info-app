package gds.com.studentinformation.utils.retrofit;

import gds.com.studentinformation.models.Result;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIService {

    @GET("check_admin_records.php")
    Call<Result> checkIfThereIsAnAdmin();

    @GET("student_info.php")
    Call<Result> getStudentDetails(@Query("email") String email);

    @GET("admin_info.php")
    Call<Result> getAdminDetails(@Query("email") String email);

    @GET("find_all_students.php")
    Call<Result> getAllStudents();

    @FormUrlEncoded
    @POST("user_register.php")
    Call<Result> saveUserCredentials(
            @Field("email") String email,
            @Field("password") String password,
            @Field("encrypted_password") String encrypted_password,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("admin_register.php")
    Call<Result> saveAdminCredentials(
            @Field("firstName") String firstName, @Field("middleName") String middleName,
            @Field("lastName") String lastName, @Field("email") String email,
            @Field("registrationTimeStamp") String registrationTimeStamp);

    @FormUrlEncoded
    @POST("student_register.php")
    Call<Result> saveStudentCredentials(
            @Field("firstName") String firstName, @Field("middleName") String middleName,
            @Field("lastName") String lastName, @Field("program") String program,
            @Field("yearLevel") String yearLevel, @Field("email") String email,
            @Field("birthday") String birthday, @Field("age") String age,
            @Field("address") String address,
            @Field("registrationTimeStamp") String registrationTimeStamp);

    @FormUrlEncoded
    @POST("login.php")
    Call<gds.com.studentinformation.models.Result> getLoginCredentials(
            @Field("email") String email,
            @Field("password") String password,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("update_student.php")
    Call<Result> updateStudent(@Field("firstName") String firstName, @Field("middleName") String middleName,
                               @Field("lastName") String lastName, @Field("program") String program,
                               @Field("yearLevel") String yearLevel,
                               @Field("birthday") String birthday, @Field("age") String age,
                               @Field("address") String address, @Field("studentId") Integer studentId);

    @DELETE("delete_student.php")
    Call<Result> deleteStudent(@Query("email") String email, @Query("studentId") Integer studentId);
}
