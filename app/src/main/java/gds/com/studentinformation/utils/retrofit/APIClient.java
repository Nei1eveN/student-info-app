package gds.com.studentinformation.utils.retrofit;

import android.content.Context;


import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static gds.com.studentinformation.utils.Constants.cacheSize;
import static gds.com.studentinformation.utils.Constants.isNetworkAvailable;
import static gds.com.studentinformation.utils.Constants.webhost_url;

public class APIClient {

    public static Retrofit getClient(final Context context) {
        Cache cache = new Cache(context.getCacheDir(), cacheSize);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    if (!isNetworkAvailable(context)) {
                        int maxStale = 60 * 60 * 24 * 28;
                        request = request
                                .newBuilder()
                                .header("cache-control", "public, only-if-cached,max-stale=" + maxStale)
                                .build();
                    }
                    return chain.proceed(request);
                }).build();

        return new Retrofit.Builder()
                .baseUrl(webhost_url)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
